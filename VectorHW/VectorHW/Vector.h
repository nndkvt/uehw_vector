

#pragma once
class Vector
{
private:
	float x, y, z;
public:
	Vector();

	Vector(float _x, float _y, float _z);

	void DisplayVector();

	void CalculateLength();
};
