#include "Vector.h"
#include <math.h>
#include <iostream>

Vector::Vector() : x(0), y(0), z(0)
{}

Vector::Vector(float _x, float _y, float _z) : x(_x), y(_y), z(_z)
{}

void Vector::DisplayVector() 
{
	std::cout << "Vector: (" << x << ", " << y << ", " << z << ")\n";
}

void Vector::CalculateLength() 
{
	float length = sqrt(x * x + y * y + z * z);
	std::cout << "Vector length: " << length << "\n";
}