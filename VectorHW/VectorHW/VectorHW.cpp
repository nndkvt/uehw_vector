﻿#include <iostream>
#include "Vector.h"

int main()
{
    float x, y, z;

    std::cout << "Enter x value of Vector: ";
    std::cin >> x;

    std::cout << "Enter y value of Vector: ";
    std::cin >> y;

    std::cout << "Enter z value of Vector: ";
    std::cin >> z;

    std::cout << "\n";

    Vector vec = Vector(x, y, z);

    vec.DisplayVector();
    vec.CalculateLength();

    return 0;
}
